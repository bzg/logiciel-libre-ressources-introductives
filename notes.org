# -*- mode: org; coding: utf-8; org-confirm-babel-evaluate: nil -*-
#+title: Implémentation des remarques du document de travail
#+author: Leo Vivier
#+language: fr
#+date: 2020-11-02

* Points à adresser en priorité

Tous les TODOs
#+begin_src elisp :results none
(org-overview)
(org-tags-sparse-tree t "*")
#+end_src

Questions trop pointues
#+begin_src elisp :results none
(org-overview)
(org-tags-sparse-tree nil "trop_pointue")
#+end_src

Redites
#+begin_src elisp :results none
(org-overview)
(org-tags-sparse-tree nil "redite")
#+end_src

* Modifications des questions déjà présentes dans le document

** [[file:faq.org::*Quelle différence entre les licences permissives et à réciprocité ?][Quelle différence entre les licences permissives et à réciprocité ?]]

J’ai rajouté le tableau des licences libres présent dans le [[https://joinup.ec.europa.eu/sites/default/files/inline-files/FAQ-LL-V131-FR.pdf][Guide
pratique d'usage des logiciels libres dans les administrations]]
(pp. 10--11) en réparant les liens vers opensource.org.

** [[file:faq.org::*Quels sont les enjeux pour les pouvoirs publics ?][Quels sont les enjeux pour les pouvoirs publics ?]]

J’ai combiné ‘souveraineté/autonomie’ comme François l’a fait sur son
document, mais je pense qu’il faudrait revoir la formulation.

On reparle de souveraineté plus bas dans [[file:faq.org::*Pour un établissement public, quels sont les principaux avantages à libérer son code logiciel ?][Pour un établissement public,
quels sont les principaux avantages à libérer son code logiciel ?]].

** [[file:faq.org::*Quelles sont les associations de promotion du logiciel libre ?][Quelles sont les associations de promotion du logiciel libre ?]]

Ajout de l’[[file:faq.org::*AFUL][AFUL]] et de l’[[file:faq.org::*ADULLACT][ADULLACT]].

** [[file:faq.org::*L'administration publique recommande-t-elle des logiciels libres ?][L'administration publique recommande-t-elle des logiciels libres ?]]

François voulait rajouter la circulaire Ayrault, mais on la mentionne
déjà dans la question [[file:faq.org::*La loi encourage-t-elle l'administration à utiliser des logiciels libres ?][La loi encourage-t-elle l'administration à
utiliser des logiciels libres ?]].

** [[file:faq.org::*L'administration publique recommande-t-elle des logiciels libres ?][L'administration publique recommande-t-elle des logiciels libres ?]]

Ajout des deux guides publiés par l’ATICA et l’ADAE.



* Nouvelles questions

Les questions ont été ajoutées dans l’ordre suggéré par François.

** [[file:faq.org::*Quel est le système d'exploitation qui tourne sur les 500 plus gros ordinateurs du monde ?][Quel est le système d'exploitation qui tourne sur les 500 plus gros ordinateurs du monde ?]]

** [[file:faq.org::*Pourquoi Microsoft est il le premier contributeur du libre ?][Pourquoi Microsoft est il le premier contributeur du libre ?]]

** [[file:faq.org::*Pour un établissement public, quels sont les principaux avantages à libérer son code logiciel ?][Pour un établissement public, quels sont les principaux avantages à libérer son code logiciel ?]]

** [[file:faq.org::*Le monde universitaire a-t-il une proximité et des valeurs partagées avec le monde du logiciel libre ?][Le monde universitaire a-t-il une proximité et des valeurs partagées avec le monde du logiciel libre ?]]

** TODO [[file:faq.org::*Des éditeurs privés ont-ils déjà libéré leur code logiciel ? Pourquoi ?][Des éditeurs privés ont-ils déjà libéré leur code logiciel ?  Pourquoi ?]] :trop_pointue:

Question légèrement reformulée par rapport à celle suggérée.

Mes connaissances sur le sujet sont trop faibles pour formuler une réponse.

** [[file:faq.org::*Le monde universitaire a-t-il une proximité et des valeurs partagées avec le monde du logiciel libre ?][Le monde universitaire a-t-il une proximité et des valeurs partagées avec le monde du logiciel libre ?]]

Développement plus poussé que celui suggéré, notamment via l’inclusion de
citations tirés de l’Open Science.

** [[file:faq.org::*Peut on mixer les concepts de /copyleft/ et /copyright/ ?][Peut on mixer les concepts de /copyleft/ et /copyright/ ?]]

Reprise de la formulation proposée.

** TODO [[file:faq.org::*Quelles solutions d'état ont engagé leur transition vers l’Open Source ? Quelles solutions le sont nativement ?][Quelles solutions d'état ont engagé leur transition vers l’Open Source ?  Quelles solutions le sont nativement ?]] :trop_pointue:

Malgré la liste fournie par François (et répliquée dans les sources de la
question), je n’arrive pas à juger la saillance des différents projets.  Je
pense que Bastien sera plus à même de faire une sélection intéressante.

** [[file:faq.org::*Pour un établissement public, quels sont les principaux avantages à libérer son code logiciel ?][Pour un établissement public, quels sont les principaux avantages à libérer son code logiciel ?]]

Reprise des points détaillés que François avait mentionnés dans un ancien
email.  Ils n’apparaissent pas dans la dernière version du document qu’il nous
a envoyé le <2020-10-27 Tue>, mais je pense que c’est un oubli.

** TODO [[file:faq.org::*Et pour un établissement public qui construit des solutions logicielles et offre des services autour de celles-ci, quels sont les avantages à libérer son code logiciel ?][Et pour un établissement public qui construit des solutions logicielles et offre des services autour de celles-ci, quels sont les avantages à libérer son code logiciel ?]] :redite:

cf. point suivant.

** [[file:faq.org::*Pour un établissement public qui construit des solutions logicielles libres et offre des services autour de celles-ci, quel est le meilleur moment pour libérer le code de ses logiciels ?][Pour un établissement public qui construit des solutions logicielles libres et offre des services autour de celles-ci, quel est le meilleur moment pour libérer le code de ses logiciels ?]]

Dans la dernière version de François, une question très similaire (mais plus
large) est abordée juste avant celle-ci : [[file:faq.org::*Et pour un établissement public qui construit des solutions logicielles libres et offre des services autour de celles-ci, quels sont les principaux avantages à libérer son code logiciel ?][Et pour un établissement public qui
construit des solutions logicielles libres et offre des services autour de
celles-ci, quels sont les principaux avantages à libérer son code logiciel ?]].

J’ai décidé de les combiner, notamment parce que je ne suis pas sûr des
subtilités qu’il y aurait avec la question [[file:faq.org::*Pour un établissement public, quels sont les principaux avantages à libérer son code logiciel ?][Pour un établissement public, quels
sont les principaux avantages à libérer son code logiciel ?]]
(i.e. ‘établissement public’ vs. ‘établissement public qui construit des
solutions logicielles libres’).  S’il faut développer, François évoque les
pistes suivantes : ‘Notion d'accélération de la construction, de la
sécurisation, du déploiement.  Il fait des économies de R&D’.  D’autres
éléments sont développés dans [[file:faq.org::*Pourquoi choisir un logiciel libre plutôt qu’un autre ?][Pourquoi choisir un logiciel libre plutôt qu’un
autre ?]] (question /commented out/, sur la perspective utilisateur).

** [[file:faq.org::*Pour un établissement public qui construit des solutions logicielles libres et offre des services autour de celles-ci, quel est le meilleur moment pour libérer le code de ses logiciels ?][Pour un établissement public qui construit des solutions logicielles libres et offre des services autour de celles-ci, quel est le meilleur moment pour libérer le code de ses logiciels ?]]

Reprise d’une formulation présente dans un document antérieur.

Reformulation légère de ce que François a proposé dans son dernier document.

** [[file:faq.org::*Pour un établissement public qui construit des solutions logicielles et offre des services autour de celles-ci, comment opérer sa transition d'un modèle économique basé sur ses logiciels propriétaires vers un modèle économique appuyé sur ses logiciels libres ?][Pour un établissement public qui construit des solutions logicielles et offre des services autour de celles-ci, comment opérer sa transition d'un modèle économique basé sur ses logiciels propriétaires vers un modèle économique appuyé sur ses logiciels libres ?]]

Reprise des points de François avec étoffement.

** TODO [[file:faq.org::*Peut on rendre libre le code logiciel spécifique fait avec la technologie d'un ou les paramétrages de ce même ERP (ex : SAP, HrAccess, Oracle application, etc.) ?][Peut on rendre libre le code logiciel spécifique fait avec la technologie d'un ou les paramétrages de ce même ERP (ex : SAP, HrAccess, Oracle application, etc.) ?]] :trop_pointue:

La question présente trop de zones d’ombre (à l’aune de mes compétences) pour
que je puisse répondre sereinement.

** [[file:faq.org::*Peut-on imaginer un dispositif qui permette d'ouvrir le code mais restreigne l'usage de la solution (une licence « ouverts aux établissements ESR ») ?][Peut-on imaginer un dispositif qui permette d'ouvrir le code mais restreigne l'usage de la solution (une licence « ouverts aux établissements ESR ») ?]]

La formulation proposée par François dans le dernier document est ‘Peut on
restreindre l'usage d'une solution logicielle au code ouvert ?’, mais je
trouve la précédente version qu’il avait suggérée plus précise.

J’ai reformulé légèrement les paragraphes de François.  J’ai laissé le dernier
paragraphe, mais il ne fait pas particulièrement sens pour moi.

** TODO [[file:faq.org::*Comment, dans une stratégie d'ouverture, protéger une partie du code sensible (ex : contrôle N° Insee, flux financiers par CB, etc.) ?][Comment, dans une stratégie d'ouverture, protéger une partie du code sensible (ex : contrôle N° Insee, flux financiers par CB, etc.) ?]] :trop_pointue:

La question est trop pointue pour que je puisse y répondre, notamment à cause
du ‘comment’.  Peut-être que le type de document ne nécessite pas de
détailler, auquel cas nous ne nous intéresserions qu’à une réponse brève en
oui/non ?

Vos échanges sur le document de travail :

#+begin_quote
fonctionnalités sensibles : dans notre code nous avons des algorithmes
liés à des contrôles de sécurité sociale, contrôle de flux financiers
par CB,...a priori nos partenaires (CPAM par ex) demandent que ces
algorithmes restent secrets.  Ces sous-ensembles de code peuvent-ils
être exclus de l'ouverture ? @Bastien : corriger si besoin :

Oui c'est possible, en modularisant, et en faisant que les modules
concernés ne soient pas concernés juridiquement par l'ouverture

Cela rend il « non ouvrables » tous les codes utilisant cette fonction?
Même nature de question si un composant ne peut être open source pour
des raisons de technologie propriétaire : quel sens d'ouvrir le code des
modules qui l'utilisent ?

Il existe des moyens techniques et juridiques de faire cohabiter
- du code /copyleft/ fort
- du code /copyleft/ faible
- du code propriétaire

C’est une raison de plus pour modulariser!
#+end_quote

** TODO [[file:faq.org::*Pour un établissement public qui construit des solutions logicielles et offre des services autour de celles-ci, comment contenir le risque d'avoir la création de services concurrents à partir du code ouvert ?][Pour un établissement public qui construit des solutions logicielles et offre des services autour de celles-ci, comment contenir le risque d'avoir la création de services concurrents à partir du code ouvert ?]] :trop_pointue:

Je ne me sens pas à l’aise pour répondre aux questions spécifiques à l’AMUE.
Elles demandent un positionnement stratégique précis que vous avez sûrement
abordé en réunion, mais dont la trace sur votre document de travail ne me
permet pas de me faire une idée claire.

Ladite trace :
#+begin_quote
Le meilleur moyen de ne pas craindre ces services concurrents, c'est de s'arranger pour qu'ils deviennent intéressants dans le modèle cible. 
 où serait l'intérêt (forcément limité - pas question de gérer un fork hostile, qui aurait perdu d'avance) d'une entreprise sur ce code   ouvert ? Que pourrait-elle vendre comme service ? 
- quel serait l'intérêt de l'AMUE à ce qu'existe hors d'elle un écosystème de ce genre ? Comment pourrait-elle travailler à le  réguler ?
[PK] le choix de la licence peut aider...

Celui qui gère le droit en écriture dans le trunk d’un logiciel qui a une communauté active n’a rien à craindre d’un fork. 

Sous partie sur la notion de “marché” fermé (les établissements français ESR), la réponse de Stéphane Athanase était : pas d'inquiétude majeure sur l'impact de notre modèle économique. En effet les établissements, avec l'Amue, investissent pour un usage des solutions pas pour posséder le code logiciel. Message fort de notre directeur.
C’est exactement le discours des collectivités au sein de l’Adullact. C’est ainsi qu’on été financées les grandes encyclopédies: par souscription: on vise à “faire exister” quelque chose pour tous, par à le poséder.
Suite réunion 23/10 :         fork : notre version sera plus vivante qu'une version concurrente. et c'est à l'éditeur d'expliquer à ses membres la bonne manière de faire...


 Seconde sous partie de réponse, sur “en dehors de nos frontières” : risque que nos solutions soient utilisées en dehors de nos frontières ne pose pas de problème majeur puisque nous pourrions bénéficier de retours, de contribution grace au choix d'une bonne licence.
Suite réunion 23/10 : une offre pourrait naitre, il faut s'assurer de "caper"/limiter les incidences sur nos charges.
#+end_quote

** TODO [[file:faq.org::*Pour un établissement public qui construit des solutions logicielles et offre des services autour de celles-ci, quelles sont les opportunités d'ouvrir son code logiciel ?][Pour un établissement public qui construit des solutions logicielles et offre des services autour de celles-ci, quelles sont les opportunités d'ouvrir son code logiciel ?]] :redite:

Avantages vs. opportunités ?  cf. [[file:faq.org::*Et pour un établissement public qui construit des solutions logicielles et offre des services autour de celles-ci, quels sont les avantages à libérer son code logiciel ?][Et pour un établissement public qui
construit des solutions logicielles et offre des services autour de celles-ci,
quels sont les *avantages* à libérer son code logiciel ?]]

Pour le moment, j’ai /commented out/ la question.

Vos notes :
#+begin_quote
les opportunités :  
inciter les établissements à contribuer à la maintenance, l'évolution de nos solutions plus encore qu'ils ne le font ? Dans ce cas, notre valeur serait d'accompagner (par notre connaissance du code, du fonctionnel) 
oui, la vraie valeur est
- dans la connaissance du code
- dans la maîtrise de la feuille de route
[PK] c'est ce que fait l'ADULLACT avec les collectivités. ça passe par des ateliers destinés à prioritiser les évolutions.

Ouvrir le code, c’est ouvrir vraiment la feuille de route, et impliquer les utilisateurs dans la réalisation des évolutions qu’ils demandent.
#+end_quote

** TODO [[file:faq.org::*Pour un établissement public qui construit des solutions logicielles et offre des services autour de celles-ci, quels éléments pour décider d'ouvrir le code de solutions anciennes ? Comment, dans ce cas, limiter les risques de freiner la diffusion de la solution remplaçante ?][Pour un établissement public qui construit des solutions logicielles et offre des services autour de celles-ci, quels éléments pour décider d'ouvrir le code de solutions anciennes ?  Comment, dans ce cas, limiter les risques de freiner la diffusion de la solution remplaçante ?]] :trop_pointue:

Vos notes :
#+begin_quote
Exemple :  Nous avons deux solutions majeurs dont on annonce la fin de maintenance : la solution de RH (arpège) est en cours de remplacement par une solution ERP Siham. La solution Apogée (scolarité) sera remplacée par la solution, en cours de développement PcScol/Pégase (fin de vie d'apogée 2025). Faut-il ouvrir Apogée ou Harpège pour leur maintenance fin de vie. Argument du relatif intérêt des établissements à investir pour maitriser une solution qui va devenir technologiquement et fonctionnellement obsolète. Hypothèse de créer des centres de compétences. L'ouverture des anciennes solutions est elle un risque sur le déploiement des nouvelles solutions.
    [PK] au contraire: l'abandon des anciennes solutions est souvent l'occasion de passer au libre. Tant qu'à changer autant changer intelligemment... d'autant que les solutions propriétaires remplacantes sont souvent en mode SAAS, cad qu'on perd de plus en plus le controle du logiciel mais aussi et surtout de ses données.
Suite réunion 23/10 :         biseau en fin de solution et démarrage de la nouvelle : opportunité de transférer la maintenance mais risque de ralentir le déploiement de la nouvelle. C'est la qualité de la nouvelle offre qui prévaut pour la "bascule", sa valeur ajoutée (usage, périmètre, performance,...)
#+end_quote

** TODO [[file:faq.org::*Pour un établissement public qui construit des solutions logicielles et offre des services autour de celles-ci, comment s'assurer qu'un établissement utilise la version « garantie » dans le cadre des services d'assistance ?][Pour un établissement public qui construit des solutions logicielles et offre des services autour de celles-ci, comment s'assurer qu'un établissement utilise la version « garantie » dans le cadre des services d'assistance ?]]

Reprise des éléments du document de travail.

Peut-être à étoffer ?

** [[file:faq.org::*Pour un établissement public qui construit des solutions logicielles et offre des services autour de celles-ci, le passage en mode /cloud/ a-t-il une incidence sur le choix des licences?][Pour un établissement public qui construit des solutions logicielles et offre des services autour de celles-ci, le passage en mode /cloud/ a-t-il une incidence sur le choix des licences?]]

Reformulation des notes du document de travail.
